<?php
$user = 'students-website';
$password = 'stw-psk-2022';
$db = 'studentsdb';
$connection = new mysqli('localhost', $user, $password, $db) ;
if ($connection->connect_error) {
    $error = array('status' => false, 'errorMessage' => 'Failed connection to database');
    echo json_encode($error);
    $connection->close();
    exit();
}

// get all students from table
$query = "SELECT * FROM students";

$result = mysqli_query($connection, $query);
if ($result === false) {
    $error = array('status' => false, 'errorMessage' => 'Failed to load students from table');
    echo json_encode($error);
    $connection->close();
    exit();
}

// put records from table to array
$students = array();
while ($row = $result->fetch_assoc()) {
    $students[] = $row;
}

$studentsCount = $connection->query('SELECT MAX(`id`) as count FROM `students`')->fetch_assoc()['count'];

$data = array ('status' => true, 'students' => $students, 'count' => $studentsCount);

$connection->close();

echo json_encode($data);
?>