window.addEventListener('load', async () =>
{
  if('serviceWorker' in navigator)
  {
    try 
    {
      //register a service worker
      const reg = await navigator.serviceWorker.register('sw.js');
      console.log("SW success", reg);
    } 
    catch (error) 
    {
      console.log("SW fail");
      console.log(error);
    }
}
});


var addStudentModal = document.getElementById("add-student");
var deleteStudentModal = document.getElementById("delete-student");
var editStudentModal = document.getElementById("edit-student");
var rowToDelete;
var rowToEdit;

function addStudentClick()
{
    addStudentModal.style.display = "block";
}

function addStudentCloseClick()
{
    addStudentModal.children[0].children[1].innerText = "Add student...";
    document.getElementById("add-submit").setAttribute("onclick","addStudentOKClick()");
    addStudentModal.style.display = "none";
}

function deleteStudentClick(element)
{
    rowToDelete = element.parentElement.parentElement;
    deleteStudentModal.style.display = "block";
}

function deleteStudentCloseClick()
{
    deleteStudentModal.style.display = "none";
}

window.onclick = function(event) {
    if (event.target.className.includes("modal")) {
      event.target.style.display = "none";
    }
} 

function addStudentOKClick()
{
    var newRow=document.createElement("tr");
    var checkbox=document.createElement("input");
    checkbox.setAttribute("type","checkbox");
    var checkboxCell=document.createElement("td");
    checkboxCell.appendChild(checkbox);
    var group=document.getElementById("igroup").value;
    var groupCell=document.createElement("td");
    var groupH5 = document.createElement("h5");
    groupH5.innerText=group;
    groupCell.appendChild(groupH5);
    var name=document.getElementById("ifname").value+" "+document.getElementById("ilname").value;
    var nameCell=document.createElement("td");
    var nameH5 = document.createElement("h5");
    nameH5.innerText=name;
    nameCell.appendChild(nameH5);
    var gender=document.getElementById("igender").value;
    var genderCell=document.createElement("td");
    var genderH5 = document.createElement("h5");
    genderH5.innerText=gender;
    genderCell.appendChild(genderH5);
    var birthdate=document.getElementById("ibirthdate").value;
    var birthdateCell=document.createElement("td");
    var birthdateH5 = document.createElement("h5");
    birthdateH5.innerText=birthdate;
    birthdateCell.appendChild(birthdateH5);
    var status=document.createElement("span");
    status.setAttribute("class","status-offline");
    status.setAttribute("ondblclick","changeStatus(this)");
    var statusCell=document.createElement("td");
    statusCell.appendChild(status);
    var editButton = document.createElement("button");
    var editIcon = document.createElement("i");
    editIcon.setAttribute("class","bi bi-pencil-square options-icon");
    editButton.appendChild(editIcon);
    editButton.setAttribute("class","options-button");
    editButton.setAttribute("onclick","editStudentClick(this)");
    var deleteButton = document.createElement("button");
    var deleteIcon = document.createElement("i");
    deleteIcon.setAttribute("class", "bi bi-x-square options-icon");
    deleteButton.appendChild(deleteIcon);
    deleteButton.setAttribute("class", "options-button");
    deleteButton.setAttribute("onclick","deleteStudentClick(this)");
    var optionsCell=document.createElement("td");
    optionsCell.appendChild(editButton);
    optionsCell.appendChild(deleteButton);
    newRow.appendChild(checkboxCell);
    newRow.appendChild(groupCell);
    newRow.appendChild(nameCell);
    newRow.appendChild(genderCell);
    newRow.appendChild(birthdateCell);
    newRow.appendChild(statusCell);
    newRow.appendChild(optionsCell);
    document.getElementById("students-table").appendChild(newRow);
    addStudentModal.style.display = "none";
    var id = document.getElementById("hidden-input");
    var formData = {id, group, name, gender, birthdate};
    var jsonData = JSON.stringify(formData);
    console.log(jsonData);
}

function deleteStudentOKClick()
{
    rowToDelete.remove();
    deleteStudentModal.style.display = "none";
}

function checkAll(element)
{
    var table = document.getElementById("students-table");
    for (var i = 1, row; row = table.rows[i]; i++)
    {
        if (element.checked)
            row.cells[0].children[0].checked = true;
        else row.cells[0].children[0].checked = false;
    }
}

function changeStatus(element)
{
    if(element.getAttribute("class") == "status-online")
        element.setAttribute("class", "status-offline");
    else element.setAttribute("class", "status-online");
}

function launchAnimation()
{
    document.getElementById("bell-indicator").style.display="inline-block";
}

function editStudentClick(element)
{
    rowToEdit = element.parentElement.parentElement;
    document.getElementById("igroup").value = rowToEdit.cells[1].children[0].innerText;
    var names = rowToEdit.cells[2].children[0].innerText.split(" ");
    document.getElementById("ifname").value = names[0];
    document.getElementById("ilname").value = names[1];
    document.getElementById("igender").value = rowToEdit.cells[3].children[0].innerText;
    document.getElementById("ibirthdate").value = rowToEdit.cells[4].children[0].innerText;
    addStudentModal.children[0].children[1].innerText = "Edit student...";
    document.getElementById("add-submit").setAttribute("onclick","editStudentOKClick()");
    addStudentModal.style.display = "block";
}

function editStudentOKClick()
{
    rowToEdit.cells[1].children[0].innerText = document.getElementById("igroup").value;
    rowToEdit.cells[2].children[0].innerText = document.getElementById("ifname").value+" "+document.getElementById("ilname").value;
    rowToEdit.cells[3].children[0].innerText = document.getElementById("igender").value;
    rowToEdit.cells[4].children[0].innerText = document.getElementById("ibirthdate").value;
    addStudentModal.children[0].children[1].innerText = "Add student...";
    document.getElementById("add-submit").setAttribute("onclick","addStudentOKClick()");
    addStudentModal.style.display = "none";
}

var form = document.getElementById("add-form");
function handleForm(event) { event.preventDefault(); } 
form.addEventListener('submit', handleForm);