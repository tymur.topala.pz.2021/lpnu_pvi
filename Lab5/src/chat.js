var user;
var otherUsers =[];
var usersInChat = [];
var currentRoom;
var groups = [];
var logInModal = document.getElementById("log-in");
var registerModal = document.getElementById("register");
var addChatModal = document.getElementById("add-chat");
var addMembersModal = document.getElementById("add-members");
var messageForm = document.getElementById("message-form");
var messageInput = document.getElementById("input-message");
var socket = io.connect();

if (sessionStorage.getItem("currentUser"))
{
  user = JSON.parse(sessionStorage.getItem("currentUser"));
  console.log("User in storage: ");
  console.log(user);
  document.getElementById("profile-name").innerText = user.name;
  document.getElementById("navbar-dropdown").children[0].innerText = "Profile";
  document.getElementById("navbar-dropdown").children[0].removeAttribute("onclick");
  document.getElementById("navbar-dropdown").children[1].innerText = "Log out";
  document.getElementById("navbar-dropdown").children[1].setAttribute("onclick", "logOutClick()");
  loadAll();
}
else logInClick();

function logInClick()
{
    logInModal.style.display = "block";
}

function logInCloseClick()
{
    logInModal.style.display = "none";
}

function logInOKClick()
{
    var userData = {
        email: document.getElementById("login-email").value,
        password: document.getElementById("login-password").value
    };
    axios.post("/logIn", userData).then(response => {
        console.log(response.data);
        user = response.data;
        sessionStorage.setItem("currentUser",JSON.stringify(user));
        document.getElementById("profile-name").innerText = user.name;
        logInModal.style.display = "none";
        document.getElementById("navbar-dropdown").children[0].innerText = "Profile";
        document.getElementById("navbar-dropdown").children[0].removeAttribute("onclick");
        document.getElementById("navbar-dropdown").children[1].innerText = "Log out";
        document.getElementById("navbar-dropdown").children[1].setAttribute("onclick", "logOutClick()");
        loadAll();
    }).catch (error => {
        console.error(error);
        alert(error);
    });
}

function logOutClick()
{
  sessionStorage.setItem("currentUser","");
  location.reload();
}

function registerClick()
{
    registerModal.style.display = "block";
}

function registerCloseClick()
{
    registerModal.style.display = "none";
}

function registerOKClick()
{
    const newUser = {
        name: document.getElementById("register-name").value,
        email: document.getElementById("register-email").value,
        password: document.getElementById("register-password").value
    };
    console.log(newUser);
    axios.post("/signUp",newUser).then(response => {
        alert(response.data);
    });
}

function handleForm(event) { event.preventDefault(); } 
document.getElementById("login-form").addEventListener('submit',handleForm);
document.getElementById("register-form").addEventListener('submit', handleForm);
document.getElementById("add-chat-form").addEventListener('submit',handleForm);
document.getElementById("add-members-form").addEventListener('submit',handleForm);

function loadAll()
{
    axios.get("/getRooms").then(response => {
          console.log(response);
          nameChatBox = document.getElementById("name-chats-box");
          usersContainer = document.getElementById("users-container");
          chatName = document.getElementById("chat-name");
          allMessages = document.getElementById("all-mess");
          const userChats = response.data.filter(chat => {
            return chat.members.some(member => member.email === user.email);
          });
          if (userChats.length>0)
          {
            currentRoom = userChats[0];
            chatName.textContent = userChats[0].name;
          }
          for(let i=0;i<userChats.length;i++)
          {
            console.log(userChats[i]);
            groups.push(userChats[i]._id);
            socket.emit('joinGroup', userChats[i]._id);
            console.log("Connected to "+ userChats[i].name);
            nameChatBox.insertAdjacentHTML("beforeend",
            `
            <div class="user-chat-box mb-2 bg-body-secondary" onclick=changeRoom(event) >
              <p>${userChats[i].name}</p>
            </div>
            `
            )
          }
          if (userChats.length>0)
          {
          userChats[0].members.forEach(member => {
            usersInChat.push(member);
            usersContainer.insertAdjacentHTML("beforeend",
            `
            <div class="user-box">
              <img id="user-icon" src="./img/user.png" alt="Avatar" >
              <p style="margin-top: 5px; text-align: center;">${member.name}</p>
            </div>
            `
           );
          }); 
          usersContainer.insertAdjacentHTML("beforeend",
            `
            <div class="user-box" id = "btn-load-member" onclick="addMembersClick()">
              <img id="user-icon" src="./img/plus.png" alt="+" >
              <p style="margin-top: 5px; text-align: center;">Add</p>
            </div>
            `
            );
          userChats[0].messages.forEach(message => {
            allMessages.insertAdjacentHTML("beforeend",
            `
            <div class="message-box">
              <div class="user-box">
              <img id="user-icon" src="./img/user.png" alt="Avatar" >
                <p>${message.sender}</p>
              </div>
              <div class="message-content bg-body-secondary">
                <p class="message-time">${message.timestamp}</p>
                <p>${message.content}</p>
              </div>
            </div>
            `
            );
          });
          allMessages.scrollTop = allMessages.scrollHeight;
        }
    }).catch(error => {
        alert(error);
        console.error(error);
    })
}

messageForm.addEventListener("submit", function(event) {
    event.preventDefault();
    const newMessage = {
        roomID: currentRoom._id,
        sender: user.name,
        content: document.getElementById("input-message").value,
        timestamp: new Date().toISOString().slice(0, 19).replace('T', ' '),
      };
      socket.emit('message', newMessage);
      allMessages = document.getElementById("all-mess");
      allMessages.insertAdjacentHTML("beforeend",
      `
      <div class="message-box">
        <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
          <p>${newMessage.sender}</p>
        </div>
        <div class="message-content bg-body-secondary">
          <p class="message-time">${newMessage.timestamp}</p>
          <p>${newMessage.content}</p>
        </div>
      </div>
      `
      );
      messageInput.value = "";
      allMessages.scrollTop = allMessages.scrollHeight;
});

socket.on("message", (message) => {
    console.log ("New message:",message);
    if (message.roomID == currentRoom._id)
    {
      allMessages = document.getElementById("all-mess");
      allMessages.insertAdjacentHTML("beforeend",
      `
      <div class="message-box">
        <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
          <p>${message.sender}</p>
        </div>
        <div class="message-content bg-body-secondary">
          <p class="message-time">${message.timestamp}</p>
          <p>${message.content}</p>
        </div>
      </div>
      `
      );
      allMessages.scrollTop = allMessages.scrollHeight;
    }
});

function addChatClick()
{
  addChatModal.style.display = "block";
}

function addChatCloseClick()
{
  addChatModal.style.display = "none";
}

function addChat()
{
  var chatName = document.getElementById("add-chat-name").value;
  console.log(chatName);
  const chatData = {
    name: chatName,
    members: [user],
    messages: []
  };
  console.log(chatData);
  axios.post("/createRoom", chatData).then(response => {
    console.log(response);
    location.reload();
  }).catch (error => {
    alert(error);
  });
}

function addMembersClick()
{
  axios.get("/getUsers").then(response => {
    var selectUsers = document.getElementById("add-members-select");
    selectUsers.innerHTML = "";
    console.log(currentRoom);
    otherUsers = response.data.filter(filteredUser => !Array.from(currentRoom.members).map(member => member.email).includes(filteredUser.email));
    otherUsers.forEach(userToAdd => {
      const option = document.createElement("option");
      option.text = `${userToAdd.name} (${userToAdd.email})`;
      selectUsers.appendChild(option);
    })
  })
  .catch(error => {
    console.log(error);
    alert(error);
  });
  addMembersModal.style.display = "block";
}

function addMembersCloseClick()
{
  addMembersModal.style.display = "none";
}

function addMembers()
{
  var selectUsers = document.getElementById("add-members-select");
  var selectedEmails = Array.from(selectUsers.selectedOptions).map(option => option.text.match(/\((.*?)\)/)[1]);
  var usersToAdd = otherUsers.filter(filteredUser => selectedEmails.includes(filteredUser.email));
  const dataToSend = {
    roomID: currentRoom,
    members: usersToAdd
  };
  axios.post("/addMembers", dataToSend).then(response => {
    console.log(response);
    location.reload();
  }).catch(error => {
    console.error(error);
    alert(error);
  });
}

function leaveRoom()
{
  const leaveData = {
    roomID: currentRoom._id,
    email: user.email
  };
  axios.post("/leaveRoom", leaveData).then(response => {
    console.log(response);
    location.reload();
  }).catch(error => {
    console.error(error);
    alert(error);
  });
}

function removeRoom()
{
  const removeData = {
    roomID: currentRoom._id
  };
  axios.post("/removeRoom", removeData).then(response => {
    console.log(response);
    location.reload();
  }).catch(error => {
    console.error(error);
    alert(error);
  });
}

function changeRoom(event)
{
  var newChatName = {
    name: event.currentTarget.children[0].innerText
  };
  axios.post("/getSelectRoom", newChatName).then(response => {
    const chat = response.data;
    currentRoom = chat;
    console.log("User changed room to "+currentRoom.name);
    nameChatBox = document.getElementById("name-chats-box");
    usersContainer = document.getElementById("users-container");
    usersContainer.innerHTML = "";
    chatName = document.getElementById("chat-name");
    allMessages = document.getElementById("all-mess");
    allMessages.innerHTML = "";
    chatName.textContent = chat.name;
    usersInChat = [];
    chat.members.forEach(member => {
      usersInChat.push(member);
      usersContainer.insertAdjacentHTML("beforeend",
        `
        <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
          <p style="margin-top: 5px; text-align: center;">${member.name}</p>
        </div>
        `
        );
    });
    usersContainer.insertAdjacentHTML("beforeend",
      `
      <div class="user-box" id = "btn-load-member" onclick="addMembersClick()" >
        <img id="user-icon" src="./img/plus.png" alt="Avatar" >
        <p style="margin-top: 5px; text-align: center;">Add</p>
      </div>
      `
    );
    chat.messages.forEach(message => {
      allMessages.insertAdjacentHTML("beforeend",
        `
        <div class="message-box">
          <div class="user-box">
          <img id="user-icon" src="./img/user.png" alt="Avatar" >
            <p>${message.sender}</p>
          </div>
          <div class="message-content bg-body-secondary">
            <p class="message-time">${message.timestamp}</p>
            <p>${message.content}</p>
          </div>
        </div>
        `
      );
    });
    allMessages.scrollTop = allMessages.scrollHeight;
  }).catch(error => {
    console.error(error);
    alert(error);
  });
}