const mongoose = require("mongoose");
const url = "mongodb://127.0.0.1:27017/chatdb";
var express = require("express");
var app = express();
var http = require("http");
const { stringify } = require("querystring");
var server = http.createServer(app);
var io = require("socket.io")(server);

server.listen(3000);

app.use(express.json());
app.use(express.static(__dirname));

mongoose.connect(url).then(res => console.log("Connected to DB")).catch(err => console.log(err));

const User = mongoose.model('User', {
    name: String,
    email: String,
    password: String
});

const Room = mongoose.model('Room', {
    name: String,
    members: [{ name: String, email: String }],
    messages: [{sender: String, content: String, timestamp: String}]
});

app.get("/", function(req,res) {
    res.sendFile(__dirname+'index.html');
});

app.post('/signUp', function(req, res) {
    const userData = req.body;
    console.log(userData);
    User.findOne({ email: userData.email })
        .then(existingUser => {
            if (existingUser) {
                res.status(400).send('User already exists');
            } else {
                const user = new User(userData);
                user.save()
                    .then(() => {
                        res.send('User created successfully');
                    })
                    .catch(err => {
                        console.log(err);
                        res.status(500).send('Internal Server Error');
                    });
            }
        })
        .catch(err => {
        console.log(err);
        res.status(500).send('Internal Server Error');
        });
});

app.post("/logIn", function(req,res) {
    var userData = req.body;
    User.findOne({ email: userData.email })
        .then(existingUser => {
            if (existingUser) {
                if (userData.password==existingUser.password)
                    res.send(existingUser);
                else res.status(401).send("Incorrect password");
            } else {
                res.status(401).send("User does not exist");
            }
        })
        .catch(err => {
        console.log(err);
        res.status(500).send('Internal Server Error');
        });
});

app.get('/getRooms', function(req, res) {

    Room.find({})
        .then(rooms => {
            res.send(rooms);
        })
        .catch(error => {
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
});

app.get('/getUsers', function(req, res) {
    User.find({})
        .then(users => {
            res.send(users);
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
});

app.post('/createRoom', function(req, res) {
    const roomData = req.body;
  
    // Перевірка наявності чату з такою ж назвою
    Room.findOne({ name: roomData.name })
      .then(existingRoom => {
        if (existingRoom) {
          // Чат з такою назвою вже існує
          res.status(400).send('Room name taken');
        } else {
          // Створення нового чату
          const room = new Room(roomData);
          room.save()
            .then(() => {
              res.send(room);
            })
            .catch(err => {
              console.log(err);
              res.status(500).send('Internal Server Error');
            });
        }
      })
      .catch(err => {
        console.log(err);
        res.status(500).send('Internal Server Error');
      });
});

app.post('/getSelectRoom', function(req, res) {
    const roomData = req.body;
    Room.findOne({ name: roomData.name })
        .then(room => {
            if (room) {
                res.send(room);
            } else {
                res.status(404).send('Room not found');
            }
        })
        .catch(error => {
            // Обробка помилки
            console.error(error);
            res.status(500).send('Internal Server Error');
        }
    );
});

app.post('/leaveRoom', function(req, res) {
    const leaveUser = req.body;
    Room.findById(leaveUser.roomID)
        .then(room => {
            if (!room) {
                return res.status(404).send('Room not found');
            }
            // Пошук індексу учасника в чаті за email
            const memberIndex = room.members.findIndex(member => member.email === leaveUser.email);

            if (memberIndex === -1) {
                return res.status(404).send('User not found in the chat room');
            }

            // Видалення учасника з чату
            room.members.splice(memberIndex, 1);

            // Збереження змін у базі даних
            return room.save();
        })
        .then(savedChat => {
            res.send('User left the chat room successfully');
        })
        .catch(err => {
            console.error(err);
            res.status(500).send('Internal Server Error');
        });
});

app.post('/removeRoom', function(req, res) {
    const roomID = req.body.roomID;
    Room.deleteOne({ _id: roomID })
        .then(result => {
            if (result.deletedCount === 0) {
                return res.status(404).send('Chat not found');
            }
            res.send('Chat deleted successfully');
        })
        .catch(err => {
            console.log(err);
            res.status(500).send('Internal Server Error');
        });
});

app.post('/addMembers', function(req, res) {
    const { roomID, members } = req.body;
    Room.findById(roomID)
        .then(room => {
            if (!room) {
                return res.status(404).send('Chat not found');
            }
            room.members.push(...members);

            // Збереження змін у чаті
            room.save()
                .then(() => {
                    res.send('Members added successfully');
                })
                .catch(err => {
                    console.log(err);
                    res.status(500).send('Internal Server Error');
                });
        })
        .catch(err => {
            console.log(err);
            res.status(500).send('Internal Server Error');
        });
});

io.sockets.on('connection', function (socket) {
    console.log("User connected");

    socket.on('message', (message) => {
        console.log('New message: ', message);
        Room.findById(message.roomID)
            .then((room) => {
                room.messages.push(message);
                return room.save();
            })
            .then((savedChat) => {
                // Розсилка повідомлення всім учасникам групи
                socket.to(message.roomID).emit('message', message);
            })
            .catch((err) => {
                console.error(err);
            });
    });

    socket.on('joinGroup', (groupId) => {
        socket.join(groupId);
      });

    socket.on('disconnect', function(data) {
        console.log("User disconnected");
    });
});